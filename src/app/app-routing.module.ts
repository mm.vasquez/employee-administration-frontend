import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeAdministrationComponent } from './employee-administration/employee-administration.component';

const routes: Routes = [
  { path: '', component: EmployeeAdministrationComponent,  pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
