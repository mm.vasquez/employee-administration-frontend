export interface TypeIdentification{
    id: number;
    description: string;
}

export interface Employee {
id?:number;
firstSurname: string;
secondSurname: string;
firstName: string;
otherName: string;
country : string;
typeIdentification: TypeIdentification;
numberIdentification: string;
email?: string;
admissionDate: string;
status?: boolean;
createDate?: Date;
areaId : Area;
editionDate?:Date;
}

export interface Area{
id: number;
description: string;
}

export interface Country{
    description: string;
}