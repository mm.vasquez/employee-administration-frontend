import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import {environment} from '../../environments/environment';
import { Area, Employee, TypeIdentification } from '../app.types';


const apiUrl = environment.baseUrl;

@Injectable({
    providedIn: 'root'
})

export class EmployeeAdministrationService{

    constructor(private _httpClient: HttpClient){}

    private _employee: BehaviorSubject<Employee[] | null> = new BehaviorSubject<Employee[] | null>(null);

    getEmployees(): Observable<Employee[]>{
        return this._httpClient.get(apiUrl+'/employee/getAll').pipe(
            tap((response: any) => {
                this._employee.next(response);
            })
        );
    }

    get employees$(): Observable<Employee[] | null> {
        return this._employee.asObservable();
    }

    deleteEmployee(id:number){
        return this._httpClient.delete(`${apiUrl}/employee/delete/${id}`);
    }
}