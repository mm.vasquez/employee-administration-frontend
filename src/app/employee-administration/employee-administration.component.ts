import {AfterViewInit, Component, ViewChild, OnInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { RegisterEmployeeComponent } from '../register-employee/register-employee.component';
import { Employee } from '../app.types';
import { EmployeeAdministrationService } from './employee-administration.service';
import { EditEmployeeComponent } from '../edit-employee/edit-employee.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-employee-administration',
  templateUrl: './employee-administration.component.html',
  styleUrls: ['./employee-administration.component.css']
})
export class EmployeeAdministrationComponent implements OnInit, AfterViewInit{

  
  constructor(public dialog: MatDialog, public _employeeAdministrationService: EmployeeAdministrationService) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  displayedColumns: string[] = ['firstName', 'otherName', 'firstSurname', 'secondSurname', 'typeIdentification', 'numberIdentification', 'country', 'email', 'status', 'actions'];
  dataSource = new MatTableDataSource<Employee>();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(RegisterEmployeeComponent,{
      width: '640px',disableClose: true, data: {data: 1}
    });
  }

  openDialogEdit(employee: Employee): void {
    const dialogRef = this.dialog.open(EditEmployeeComponent,{
      width: '640px',disableClose: true, data: employee
    });
  }

  deleteEmployee(id:number){
    Swal.fire({
      title: '¿Está seguro de que desea eliminar el empleado?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí'
    }).then((result) => {
      if (result.isConfirmed) {
        this._employeeAdministrationService.deleteEmployee(id).subscribe((res: any) => {
          Swal.fire(
            '¡Eliminado!',
            'El empleado se elimino correctamente.',
            'success'
          )
          this.getEmployees();
        });
        
      }
    })
    
  }

  applyFilter(filterValue: any) {
    if (filterValue !== null) {
      this.dataSource.filter = filterValue.value.trim().toLowerCase();
    }
  }

  getEmployees() {
    this._employeeAdministrationService.employees$.subscribe((employees: Employee[] | null) => {
       if (employees) {
         this.dataSource.data = employees;
       }
   });
   this._employeeAdministrationService.getEmployees().subscribe();
 }

 

}
