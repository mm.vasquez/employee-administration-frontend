import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Area, Country, Employee, TypeIdentification } from '../app.types';
import * as moment from 'moment';
import { UpperCasePipe } from '@angular/common';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { EditEmployeeService } from './edit-employee.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

  public breakpoint!: number;
  public addEmployeeForm!: FormGroup;
  private date: Date = new Date();
  public minDate: Date = new Date();
  public maxDate: Date = this.date;
  wasFormChanged = false;
  countrys: Country[] = [
    {description: 'COLOMBIA'},
    {description: 'ESTADOS UNIDOS'}
  ];
  typesIdentification:TypeIdentification[] = [];
  areas:Area[] = [];
  today: any;
  employee!:Employee;

  public disabled = true;

  @ViewChild('picker') picker: any;

  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: Employee,
    public _editEmployee: EditEmployeeService) { }


  ngOnInit(): void {
    this.minDate.setMonth(this.minDate.getMonth()-1);
    this.addEmployeeForm = this.fb.group({
      id: this.data.id,
      firstSurname: [this.data.firstSurname,[Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]],
      secondSurname: [this.data.secondSurname,[Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]],
      firstName: [this.data.firstName,[Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]],
      otherName: [this.data.otherName,[Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]],
      country: [this.data.country,[Validators.required]],
      typeIdentification: [this.data.typeIdentification.id,[Validators.required]],
      numberIdentification: [this.data.numberIdentification,[Validators.required, Validators.pattern("^[a-zA-Z0-9-]*$")]],
      admissionDate: [this.data.admissionDate,[Validators.required]],
      status: [this.data.status,[Validators.required]],
      areaId: [this.data.areaId.id],
      createDate: [this.data.createDate],
      editionDate: [this.data.editionDate]
    });
    this.breakpoint = window.innerWidth <= 600 ? 1 : 2;

    this.getTypesIdentification();
    this.getAreas();
  }

  formChanged() {
    this.wasFormChanged = true;
  }

  public onResize(event: any): void {
    this.breakpoint = event.target.innerWidth <= 600 ? 1 : 2;
  }

  getTypesIdentification() {
    this._editEmployee.typeIdentification$.subscribe((typeIdentification: TypeIdentification[] | null) => {
       if (typeIdentification) {
         this.typesIdentification = typeIdentification;
       }
   });
   this._editEmployee.getTypesIdentification().subscribe();
 }

 getAreas() {
   this._editEmployee.area$.subscribe((areas: Area[] | null) => {
      if (areas) {
        this.areas = areas;
      }
  });
  this._editEmployee.getArea().subscribe();
}

openDialog(): void {
    this.dialog.closeAll();
}

  onSubmit(){
    var data = this.addEmployeeForm.value;
    data.firstName = data.firstName.toUpperCase();
    data.otherName= data.otherName.toUpperCase();
    data.firstSurname = data.firstSurname.toUpperCase();
    data.secondSurname = data.secondSurname.toUpperCase();
    data.typeIdentification = this.typesIdentification.filter(x => x.id == data.typeIdentification)[0];
    data.areaId = this.areas.filter(x => x.id == data.areaId)[0];
    this.employee = data;
    console.log(data);

    
    this._editEmployee.insert(this.employee).subscribe((res: any) => {
      Swal.fire({
        icon: 'success',
        title: 'Se actualizo la información correctamente',
        showConfirmButton: false,
        timer: 1500
      })
      this.dialog.closeAll();
      window.location.reload();
    });
  }
}
