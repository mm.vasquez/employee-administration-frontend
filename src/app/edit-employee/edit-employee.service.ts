import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Area, Employee, TypeIdentification } from '../app.types';
import {environment} from '../../environments/environment';

const apiUrl = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class EditEmployeeService {

  constructor(private _httpClient: HttpClient){}

  private _typeIdentification: BehaviorSubject<TypeIdentification[] | null> = new BehaviorSubject<TypeIdentification[] | null>(null);
  private _area: BehaviorSubject<Area[] | null> = new BehaviorSubject<Area[] | null>(null);

  getTypesIdentification(): Observable<TypeIdentification[]>{
    return this._httpClient.get(apiUrl+'/typeIdentification/getAll').pipe(
        tap((response: any) => {
            this._typeIdentification.next(response);
        })
    );
}

get typeIdentification$(): Observable<TypeIdentification[] | null> {
    return this._typeIdentification.asObservable();
}

getArea(): Observable<Area[]>{
    return this._httpClient.get(apiUrl+'/area/getAll').pipe(
        tap((response: any) => {
            this._area.next(response);
        })
    );
}

get area$(): Observable<Area[] | null> {
    return this._area.asObservable();
}

insert(data: Employee): Observable<any>{
  return this._httpClient.post(apiUrl+'/employee/registerEmployee', data);
}

}
