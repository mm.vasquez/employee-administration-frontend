import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import {environment} from '../../environments/environment';
import { Area, Employee, TypeIdentification } from '../app.types';


const apiUrl = environment.baseUrl;

@Injectable({
    providedIn: 'root'
})

export class RegisterEmployeeService{

    constructor(private _httpClient: HttpClient){}

    private _typeIdentification: BehaviorSubject<TypeIdentification[] | null> = new BehaviorSubject<TypeIdentification[] | null>(null);
    private _area: BehaviorSubject<Area[] | null> = new BehaviorSubject<Area[] | null>(null);
    
    getTypesIdentification(): Observable<TypeIdentification[]>{
        return this._httpClient.get(apiUrl+'/typeIdentification/getAll').pipe(
            tap((response: any) => {
                this._typeIdentification.next(response);
            })
        );
    }

    get typeIdentification$(): Observable<TypeIdentification[] | null> {
        return this._typeIdentification.asObservable();
    }

    getArea(): Observable<Area[]>{
        return this._httpClient.get(apiUrl+'/area/getAll').pipe(
            tap((response: any) => {
                this._area.next(response);
            })
        );
    }

    get area$(): Observable<Area[] | null> {
        return this._area.asObservable();
    }

    insert(data: Employee): Observable<any>{
        return this._httpClient.post(apiUrl+'/employee/registerEmployee', data);
    }

}
