import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Area, Country, Employee, TypeIdentification } from '../app.types';
import { RegisterEmployeeService } from './register-employee.service';
import * as moment from 'moment';
import { UpperCasePipe } from '@angular/common';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';
import Swal from 'sweetalert2';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['./register-employee.component.css']
})
export class RegisterEmployeeComponent implements OnInit {
  public breakpoint!: number;
  public addEmployeeForm!: FormGroup;
  private date: Date = new Date();
  public minDate: Date = new Date();
  public maxDate: Date = this.date;
  wasFormChanged = false;
  countrys: Country[] = [
    {description: 'COLOMBIA'},
    {description: 'ESTADOS UNIDOS'}
  ];
  typesIdentification:TypeIdentification[] = [];
  areas:Area[] = [];
  today: any;
  employee!:Employee;

  public disabled = true;

  @ViewChild('picker') picker: any;

  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    public _registerEmployeeService: RegisterEmployeeService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  public ngOnInit(): void {
    console.log(this.data);
    this.minDate.setMonth(this.minDate.getMonth()-1);
    this.addEmployeeForm = this.fb.group({
      IdProof: null,
      firstSurname: ['',[Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*'), Validators.maxLength(20)]],
      secondSurname: ['',[Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*'), Validators.maxLength(20)]],
      firstName: ['',[Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*'), Validators.maxLength(20)]],
      otherName: ['',[Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*'), Validators.maxLength(50)]],
      country: ['',[Validators.required]],
      typeIdentification: ['',[Validators.required]],
      numberIdentification: ['',[Validators.required, Validators.pattern("^[a-zA-Z0-9-]*$"), Validators.maxLength(20)]],
      admissionDate: ['',[Validators.required]],
      status: ['',[Validators.required]],
      areaId: [''],
      createDate: [new Date()]
    });
    this.breakpoint = window.innerWidth <= 600 ? 1 : 2;

    this.getTypesIdentification();
    this.getAreas();
    this.today = new Date();
  }
  

  public onAddCus(): void {
    this.markAsDirty(this.addEmployeeForm);
  }

  openDialog(): void {
      this.dialog.closeAll();
  }

  public onResize(event: any): void {
    this.breakpoint = event.target.innerWidth <= 600 ? 1 : 2;
  }

  private markAsDirty(group: FormGroup): void {
    group.markAsDirty();
    for (const i in group.controls) {
      group.controls[i].markAsDirty();
    }
  }

  formChanged() {
    this.wasFormChanged = true;
  }


  getTypesIdentification() {
     this._registerEmployeeService.typeIdentification$.subscribe((typeIdentification: TypeIdentification[] | null) => {
        if (typeIdentification) {
          this.typesIdentification = typeIdentification;
        }
    });
    this._registerEmployeeService.getTypesIdentification().subscribe();
  }

  getAreas() {
    this._registerEmployeeService.area$.subscribe((areas: Area[] | null) => {
       if (areas) {
         this.areas = areas;
       }
   });
   this._registerEmployeeService.getArea().subscribe();
 }

 onSubmit(){
   var data = this.addEmployeeForm.value;
   data.firstName = data.firstName.toUpperCase();
   data.otherName= data.otherName.toUpperCase();
   data.firstSurname = data.firstSurname.toUpperCase();
   data.secondSurname = data.secondSurname.toUpperCase();
   console.log(data);
   
   this.employee = data;
   this.employee.status = true;
    this._registerEmployeeService.insert(this.employee).subscribe((res: any) => {
      Swal.fire({
        icon: 'success',
        title: 'Se registro la información correctamente',
        showConfirmButton: false,
        timer: 1500
      })
      this.dialog.closeAll();
      window.location.reload();
    }, (err: HttpErrorResponse) => {
      Swal.fire(
        {
          icon: 'warning',
          title: 'Revisar campos antes de guardar',
          showConfirmButton: false,
          timer: 1500
        }
      )
    });
   
 }


}


